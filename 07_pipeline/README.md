
---

# Pipelines

---

--

# Pipelines

A Pipeline is a suite of plugins which supports implementing and integrating continuous delivery pipelines into Jenkins. A continuous delivery (CD) pipeline is an automated expression of your process for getting software from version control right through to your users and customers. Every change to your software (committed in source control) goes through a complex process on its way to being released. This process involves building the software in a reliable and repeatable manner, as well as progressing the built software (called a "build") through multiple stages of testing and deployment.


---

# Declarative vs. Scripted Pipeline syntax

A Jenkins Pipeline can be written using two types of syntax - Declarative and Scripted. Declarative and Scripted Pipelines are constructed fundamentally differently. Declarative Pipeline is a more recent feature of Jenkins Pipeline which:
- provides richer syntactical features over Scripted Pipeline syntax
- it's designed to make writing and reading Pipeline code easier.

---

# Why Pipeline?

Jenkins is, fundamentally, an automation engine which supports a number of automation patterns. Pipeline adds a powerful set of automation tools onto Jenkins, supporting use cases that span from simple continuous integration to comprehensive CD pipelines. By modeling a series of related tasks, users can take advantage of the many features of Pipeline:

- Code: Pipelines are implemented in code and typically checked into source control, giving teams the ability to edit, review, and iterate upon their delivery pipeline
- Durable: Pipelines can survive both planned and unplanned restarts of the Jenkins controller.
- Pauseable: Pipelines can optionally stop and wait for human input or approval before continuing the Pipeline run.

---

# Why Pipeline? (cont.)

- Versatile: Pipelines support complex real-world CD requirements, including the ability to fork/join, loop, and perform work in parallel.
- Extensible: The Pipeline plugin supports custom extensions to its DSL [1] and multiple options for integration with other plugins.

While Jenkins has always allowed rudimentary forms of chaining Freestyle Jobs together to perform sequential tasks,  Pipeline makes this concept a first-class citizen in Jenkins

---


# Pipeline concepts

The following concepts are key aspects of Jenkins Pipeline, which tie in closely to Pipeline syntax:

-  **Pipeline**: is a user-defined model of a CD pipeline. A Pipeline’s code defines your entire build process, which typically includes stages for building an application, testing it and then delivering it. Also, a pipeline block is a key part of Declarative Pipeline syntax.
-  **Node**: is a machine which is part of the Jenkins environment and is capable of executing a Pipeline. Also, a node block is a key part of Scripted Pipeline syntax.
-  **Stage**: defines a conceptually distinct subset of tasks performed through the entire Pipeline (e.g. "Build", "Test" and "Deploy" stages), which is used by many plugins to visualize or present Jenkins Pipeline status/progress. 
- **Step**: A single task. Fundamentally, a step tells Jenkins what to do at a particular point in time (or "step" in the process). For example, to execute the shell command make use the sh step: sh 'make'. When a plugin extends the Pipeline DSL, that typically means the plugin has implemented a new step.

---

# Declarative Pipeline fundamentals

In Declarative Pipeline syntax, the pipeline block defines all the work done throughout your entire Pipeline.
Declarative Pipeline
```groovy
pipeline {
    agent any // Execute this Pipeline or any of its stages, on any available agent. 
    stages {
        stage('Build') {  //Defines the "Build" stage.
            steps {
                //  Perform some steps related to the "Build" stage
            }
        }
        stage('Test') { //Defines the "Test" stage.
            steps {
                //  Perform some steps related to the "Test" stage
            }
        }
        stage('Deploy') { //Defines the "Deploy" stage.
            steps {
                // Perform some steps related to the "Deploy" stage
            }
        }
    }
}
```
---

# Declarative Pipeline fundamentals (cont.)
For purpose of our course we'll be suggesting some initial steps that we deem to be useful as part of best practice suggestion.
here is the pipeline that we would provide:
```groovy
pipeline{
    agent any
    stages{
        stage('PreBuild'){
            echo 'checking dependencies for the build'
            sleep 2
            // install dependencies for the project
            // not mandatory yet useful in cases where agents are dynamic and not properly configured: discussion ?!
        }
        stage('Lint'){
            echo 'checking and formatting the code'
            sleep 2
            // need to use code specific tool for checking static code analysis and format
        }
        stage('Build'){
            echo 'building the project'
            sleep 2
            // mainly used to compile binary or pack scripts in to executable
        }
```
---
# Declarative Pipeline fundamentals (cont.)
```groovy
        stage('Test'){
            echo 'testing the code'
            sleep 2
            //using developers tests on existing and added coded binary/executable to see regression/sanity/glitches 
        }
        stage('Archive'){
            echo 'archiving the code'
            sleep 2
            // mostly used to save the binary/executable into compact format
        }
    }// There are changes that will come by
}

```
---

# Practice
Create a pipeline with stages and tasks as follows below:
- prebuilt: install next list of packages
        - python3
        - python3-pip
        - python3-flask
        - pylint
        - pyinstaller
        - curl
        - wget
-  linter for testing python code with pylint
-  build step where you pack python application with pyinstaller
-  test stage to test running application with curl
-  archiving the created and packed application with `archive` stage
> Note: you can find the python app [here](https://gitlab.com/vaiolabs-io/jenkins-examples.git)

---
# Creating a Jenkinsfile

As discussed in the Defining a Pipeline in SCM, a Jenkinsfile is a text file that contains the definition of a Jenkins Pipeline and is checked into source control

```groovy
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'Building..'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing..'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying....'
            }
        }
    }
}
```

---
# Creating a Jenkinsfile (cont.)

Working with your Jenkinsfile

The following sections provide details about handling:

- specific Pipeline syntax in your Jenkinsfile features and functionality of Pipeline syntax which are essential in building your application or Pipeline project.

---

# Using environment variables

Jenkins Pipeline exposes environment variables via the global variable env, which is available from anywhere within a Jenkinsfile. The full list of environment variables accessible from within Jenkins Pipeline is documented at ${YOUR_JENKINS_URL}/pipeline-syntax/globals#env and includes:

- BUILD_ID: The current build ID, identical to BUILD_NUMBER for builds created in Jenkins versions 1.597+
- BUILD_NUMBER: The current build number, such as "153"
- BUILD_TAG:    String of `jenkins-${JOB_NAME}-${BUILD_NUMBER}`. Convenient to put into a resource file, a jar file, etc for easier identification 
- BUILD_URL:    The URL where the results of this build can be found (for example http://buildserver/jenkins/job/MyJobName/17/ )

---
# Using environment variables (cont.)

- JENKINS_URL:    Full URL of Jenkins, such as https://example.com:port/jenkins/ (NOTE: only available if Jenkins URL set in "System Configuration")
- JOB_NAME :   Name of the project of this build, such as "foo" or "foo/bar".
- NODE_NAME:    The name of the node the current build is running on. Set to 'master' for the Jenkins controller.
- WORKSPACE:    The absolute path of the workspace

---

# Using environment variables

Referencing or using these environment variables can be accomplished like accessing any key in a Groovy Map, for example:
```groovy
pipeline {
    agent any
    stages {
        stage('Example') {
            steps {
                echo "Running ${env.BUILD_ID} on ${env.JENKINS_URL}"
            }
        }
    }
}

```

---

# Practice 


---

# Pipelines with parameters

Jenkins pipelines can declare what kind of parameters it accepts and what are the defaults to those parameters
```groovy
pipeline {
    agent any
    parameters {
       string(name: 'comics', defaultValue: 'DC', description: 'name of preferred comics brand')
       booleanParam(name: 'yesno', defaultValue: false, description: 'Checkbox')
       choice(name: 'hero', choices: ['Batman', 'Superman', 'WonderWoman', 'HarleyQuinn'], description:  'Pick a hero')
       choice(name: 'villain', choices: ['Joker', 'LexLuthor', 'Cheetah', 'MentalHealth'], description:  'Pick a villain. Defaults to empty string')
       password(name: 'secret', defaultValue: '', description: 'Type some secret')
    }
    stages {
        stage('Example') {
            steps {
                echo params.comic
                echo params.yesno ? "yes" : "no"
                echo params.hero
                echo params.villain
                //echo params.secret
                echo "--------"
                echo "${params.comic}"
                echo "${params.yesno}"
                echo "${params.comic}"
                echo "${params.villain}"
                echo "${params.secret}"
                script {
                    sh "echo ${params.secret}"
            }
        }
    }
}

```

---

# Practice


---

# 