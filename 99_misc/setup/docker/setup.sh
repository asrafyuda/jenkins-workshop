#!/usr/bin/env bash


. /etc/os-release

SEPERATOR='-------------------------'


function main(){
  if [[ $ID == 'debian' ]] || [[ $ID == 'ubuntu' ]];then 
    INSTALLER='apt-get'
    deb_install
  elif [[ $ID == 'fedora' ]] || [[ $ID == 'rockylinux' ]] || [[ $ID == 'almalinux' ]];then
    INSTALLER='yum'
    rpm_install
  else 
    clear
    printf "\n%s \n%s\n %s\n" $SEPERATOR "Distro NOT Supported" $SEPERATOR
    exit 1
  fi

  sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  sudo usermod -aG docker ${USER} # need to verify who is the user

  clear

  printf "\n%s \n%s\n %s\n" $SEPERATOR "Please relogin to gain access to docker group" $SEPERATOR
}

function deb_install(){
  sudo $INSTALLER update -y
  sudo $INSTALLER install -y \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg \
      lsb-release
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

  echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

  sudo $INSTALLER update -y
  sudo $INSTALLER install -y docker-ce docker-ce-cli containerd.io
}

function rpm_install(){
  sudo $INSTALLER install -y dnf-plugins-core
  sudo $INSTALLER config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
  sudo $INSTALLER install -y  docker-ce docker-ce-cli containerd.io
}

#####
# Main
#####
main "$@"
