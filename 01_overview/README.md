
---

# Workshop Overview

---

# Let's start with basics

In order to gain most of this work shop :

- We'll setup initial lab for us to practice installation and understand bits and bytes of the installation.
- BTW, Lab will be based on this projects' material
- Use explanations, to learn CI though the work shop
- Gain practice of deployment with version control

---
# What will we do ?

So what do we have in gist? Well main bullet points would be as follows:

- We'll start by going through Jenkins installation options
- Browse Jenkins essentials
- Describe Jenkins with internals and plugins
- Speak about Jobs, Build, Pipelines and beyond
- Setup tasks and run them locally
- Create repository for code management and integrate with Jenkinsfile
- Add build agents with various types deployments

---

# What do we need ?

Essentially, our lab should to provide all we need, but it wouldn't hurt to have some experience with code and shell, running containers and also some understanding of CI.
Too much ? We'll go some of those anyway, but still it could be of help if you already have gitlab account outside of your company account.
